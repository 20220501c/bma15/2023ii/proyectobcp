/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.sguerreroc.retiroefectivo;

import java.util.Scanner;

/**
 *
 * @author Sebastián Alonso Guerrero Carrillo  <sebastian.guerrero.c@uni.pe>
 */
public class DepositoBill {

//        Construir un programa que me permita ingresar billetes
//        (en soles)
//        en una ranura con una capacidad máxima de 40 billetes
//        , para después cerrarla
    private int[] bill = new int[40];
    private int bill_10 = 0;
    private int bill_20 = 0;
    private int bill_50 = 0;
    private int bill_100 = 0;
    private int bill_200 = 0;

    public int getBill_10() {
        return bill_10;
    }

    public int getBill_20() {
        return bill_20;
    }

    public int getBill_50() {
        return bill_50;
    }

    public int getBill_100() {
        return bill_100;
    }

    public int getBill_200() {
        return bill_200;
    }

    public int[] getBill() {
        return bill;
    }

    public void testDeposBill() {
        ingresoRanura();
    }

    private void ingresoRanura() {
        Scanner input = new Scanner(System.in);

        String str;

        //Las palabras aparecen afuera del bucle para que no se repita
        System.out.println("Ingrese los billetes en la ranura (Max 40 billetes):");
        for (int i = 0; i < bill.length; i++) {

            do {
                bill[i] = input.nextInt();

                if (bill[i] == 10 || bill[i] == 20 || bill[i] == 50 || bill[i] == 100 || bill[i] == 200) {
                    break;
                } else {
                    //Aqui saldría un aviso de ERROR    
                    System.out.println("Billete no valido");
                }
            } while (true);

            //Aquí tendría que aparecer un boton de cerrar ranura
            System.out.print("Si desea cerrar la ranura, digite 'yes' ");
            str = input.next();

            if (str.equals("yes")) {
                //El boton me permite entregar menos de 40 billetes
                break;
            }
        }
        contBill();
    }

    private void contBill() {
        for (int i : bill) {
            switch (i) {
                case 10:
                    bill_10++;
                    break;
                case 20:
                    bill_20++;
                    break;
                case 50:
                    bill_50++;
                    break;
                case 100:
                    bill_100++;
                    break;
                case 200:
                    bill_200++;
                    break;

            }
        }
    }
    

}
