/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.sguerreroc.retiroefectivo;

import java.util.Scanner;

/**
 *
 * @author Sebastián Alonso Guerrero Carrillo  <sebastian.guerrero.c@uni.pe>
 */
public class DepositoMonto {

    private double total;

    public boolean testDeposMonto(DepositoBill ranura) {
        total = 10 * ranura.getBill_10() + 20 * ranura.getBill_20() + 50 * ranura.getBill_50() + 100 * ranura.getBill_100() + 200 * ranura.getBill_200();
        return mostrarMonto(ranura);
    }

    private boolean mostrarMonto(DepositoBill ranura) {

        String str;
        Scanner input = new Scanner(System.in);

        System.out.println("\nTotal ingresado por ahora :\n    S/" + (int) total);

        imprimirMonto(ranura);
        deleteBill(ranura);

        /*Aquí tendría que aparecer un boton de agregar más billetes y
           un boton para confirmar el monto( osea para terminar la operacion)
         */
        System.out.print("\nSi deseas confirmar tu monto , digite 'yes' ");
        str = input.next();
        boolean val = str.equals("yes");
        System.out.println("");

        return val;
    }

    private void imprimirMonto(DepositoBill ranura) {
        System.out.println("\n    Conformado por billetes de :");

        System.out.println("    Denominacion    Cantidad    Monto");
        if (ranura.getBill_10() != 0) {
            System.out.println("      De s/10          " + ranura.getBill_10() + "       s/" + 10 * ranura.getBill_10());
        }
        if (ranura.getBill_20() != 0) {
            System.out.println("      De s/20          " + ranura.getBill_20() + "       s/" + 20 * ranura.getBill_20());
        }
        if (ranura.getBill_50() != 0) {
            System.out.println("      De s/50          " + ranura.getBill_50() + "       s/" + 50 * ranura.getBill_50());
        }
        if (ranura.getBill_100() != 0) {
            System.out.println("      De s/100         " + ranura.getBill_100() + "       s/" + 100 * ranura.getBill_100());
        }
        if (ranura.getBill_200() != 0) {
            System.out.println("      De s/200         " + ranura.getBill_200() + "       s/" + 200 * ranura.getBill_200());
        }
    }

    private void deleteBill(DepositoBill ranura) {
        int[] bill2 = ranura.getBill();
        for (int i = 0; i < bill2.length; i++) {
            bill2[i] = 0;
        }
    }
}
