/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.sguerreroc.retiroefectivo;

/**
 *
 * @author Sebastián Alonso Guerrero Carrillo  <sebastian.guerrero.c@uni.pe>
 */
public class TestDeposito {

    public static void main(String[] args) {

        DepositoBill ranura = new DepositoBill();
        DepositoMonto calMonto = new DepositoMonto();

        do {
            ranura.testDeposBill();

            if (calMonto.testDeposMonto(ranura)) {
                break;
            }

        } while (true);

        System.out.println("Fin del programa");
    }
}
